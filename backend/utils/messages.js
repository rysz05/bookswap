const moment = require('moment');

function formatMessage(msg) {
	//console.log(moment().format('h:mm a'))
  return {
    user : msg.user,
    text: msg.text,
    chat_id : msg.chat_id,
    time: moment().format('h:mm a')
  };
}

module.exports = formatMessage;
