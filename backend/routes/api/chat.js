const express = require('express');
const router = express.Router();
const db = require('../../modules/db.js')
const chatFunction = require('../../modules/chat.js')


router.post('/socket/:chat_id', async (req, res) => {
	await dealWithMessages(req, res, 'socket')
})

router.post('/messages/:chat_id', async (req, res) => {
	await dealWithMessages(req, res, 'messages')
});



async function dealWithMessages(req, res, whichSite) {
	// configure new message
	let newMessage = {...req.body}
	let msg = {}
	msg.user = req.session.passport.user;
	msg.created_at = new Date()
	msg.chat_id = req.params.chat_id;
	msg.text = newMessage.message.text
	let chatID = msg.chat_id
	let chat = await db.findItemBy('chats', "id", chatID)

	if (msg.text !== "") {
		try {
			// adds a new message to chat

			await db.addToDatabase('messages', msg)
			try {

				let data = await chatFunction.renderChatView(req.session.passport.user, chat[0])
				let {book, renderCheckbox, my_asks, not_my_asks, messages, personalData, userData
				} = data
				db.isBookReserved(res, book)

				res.render(whichSite, {
					renderCheckbox,
						my_asks,
						not_my_asks,
						messages,
						book,
						personalData,
						chatID,
						userData
					})
			} catch(e) {console.log(e)}
		} catch(e) {console.log(e)}
	} else {
		try{
		// renders view when an empty string was send
		let data = await chatFunction.renderChatView(req.session.passport.user, chat[0])
		let {book, renderCheckbox, my_asks, not_my_asks, messages, personalData, userData
				} = data
		db.isBookReserved(res, book)

		res.render(whichSite, {
				renderCheckbox,
						my_asks,
						not_my_asks,
						messages,
						book,
						personalData,
						chatID,
						userData
				})
		} catch(e) {console.log(e)}
	}
}



module.exports = router;
