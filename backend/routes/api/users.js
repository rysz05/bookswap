const express = require('express');
const db = require('../../modules/db.js')
const router = express.Router();
const multer = require('multer');
const geo = require('../../ts-build/modules/distance.js');
const pool = require('../../database.js')
require('dotenv').config();
const moment = require('moment');
const mailjet = require('../../modules/mailjet.js');
const { v4: uuidv4 } = require('uuid');
const bcrypt = require('bcrypt');

let URL = process.env.URL
const storage = multer.diskStorage({
	destination: function(req, file, cb) {
		cb(null, './uploads/');
	},
	filename: function(req, file, cb) {
		//console.log(files)
		cb(null, file.originalname)
	}
})
const fileFilter = (req, file, cb) => {
	//reject a file
	if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
		cb(null, true);
	} else {
		cb(null, false);
	}
}
const upload = multer({
	storage: storage,
	limits: {
		fileSize: 1024*1024*5
	},
	fileFilter: fileFilter
});

// UPDATE USER
router.post('/:id/updateAddress', async (req, res) => {

	const changedData = req.body
	if (req.params.id) {
		try {
			//console.log(req.body.city)
			let address = `${req.body.street} ${req.body.building_nr}, ${req.body.city}`
			let coordinates = await geo.forwardGeocoding(address)
			if (coordinates) {
				changedData.lat = coordinates.lat
				changedData.lng = coordinates.lng

				await db.update('users', changedData, req.params.id);
				res.redirect("back")
			} else {
				console.log('bad query')
				res.redirect("back")
			}
		} catch(e) {console.log(e)}
	} else {
		res.status(400).json({msg: `No book with the id of ${req.params.id}`})
	}
})

router.post('/updateData', upload.single('img_path'), async (req, res) => {

	if (req.body) {
		const changedData = {...req.body}
		// the image is in req.file
		if(req.file) {
			changedData.img_path = req.file.path
		}
		if (req.body.street && req.body.building_nr && req.body.city) {
			let address = `${req.body.street} ${req.body.building_nr}, ${req.body.city}`
			try {
					let coordinates = await geo.forwardGeocoding(address)
					if(coordinates) {
						changedData.lat = coordinates.lat
						changedData.lng = coordinates.lng
					} else {
						console.log(coordinates)
					}
			} catch (e) {console.log(e)}
		}
			let result = await db.update('users', changedData, req.user[0].id);
			//pool.query(`UPDATE users SET updated_at = CURRENT_TIMESTAMP WHERE id = ${req.user[0].id}`)
			if(req.headers.referer.match(/[.]*\/giveMoreInfo/)) {
				res.redirect("/")
			} else {
				res.redirect("back")
			}
	} else {
		res.status(400).json({msg: 'no body'})
	}
})

router.get('/:user/validate/:token', async (req, res) => {
	let authToken = req.params.token
	let result = await pool.query(`SELECT name, authToken FROM users WHERE id = ${req.params.user}`)

	if (result[0].authToken === authToken) {
		statusChange = {authStatus: "validated"}
		await db.update('users', statusChange, req.params.user)
		let time = moment().format('MMMM Do YYYY, h:mm:ss a')
		mailjet.newUserMailNotification(result[0].name, time)
	}

	res.redirect(`${URL}/login`)
})

router.post('/sendEmail', async (req, res) => {
	let email = req.body.email;
	try {
		let result = await pool.query(`SELECT 1 FROM users WHERE email = '${email}'`)

		if(!result.length) {
			let no_account = true
            res.render('findYourAccount', {
                    no_account
                })
		} else {
			let user = await db.findItemBy('users', 'email', email)

			let code = Math.floor(Math.random()*1000000)
			var randNumb = uuidv4();

			let addToDatabase = {
				secureCode: code,
				randNumb: randNumb
			}
			let user_id = user[0].id
			await db.update('users', addToDatabase, user_id)
			mailjet.forgotPassword(user[0], code)
			res.render('recoverCode', {
				randNumb
			})
		}
	} catch(e) {console.log(e)}
})

router.post('/verifyCode/:randNumb', async (req, res) => {
	let inputCode = req.body.code
	//let user_id = req.params.user_id
	//let user = await db.findItemBy('users', 'secureCode', token)
	let randNumb = req.params.randNumb
	let secureCode = await pool.query(`SELECT secureCode FROM users WHERE randNumb = '${randNumb}'`)

	if(inputCode == secureCode[0].secureCode) {
		res.render('setNewPass', {
			randNumb
		})
	} else {
		let wrong_code = true
		res.render('recoverCode', {
			wrong_code,
			randNumb
		})
	}
})

router.post('/setNewPassword/:randNumb', async (req, res) => {

	let randNumb = req.params.randNumb
	let user_id = await pool.query(`SELECT id FROM users WHERE randNumb = '${randNumb}'`)
	user_id = user_id[0].id
	if (user_id) {
		let pass = req.body.password
		let hashedPassword = await bcrypt.hash(pass, 10)
		let updateData = {
			password: hashedPassword,
			secureCode: null,
			randNumb: null
		}

		try {
			await db.update('users', updateData, user_id)
			res.redirect(`${URL}/login`)
		} catch(e) {console.log(e)}
	} else {
		res.redirect('404')
	}
})

module.exports = router;
