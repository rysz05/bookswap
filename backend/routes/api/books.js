const express = require('express');
const router = express.Router();
const db = require('../../modules/books.js')
const db1 = require('../../modules/db.js')
const val = require('../../modules/validation.js')
const pool = require('../../database.js')

const idFilter = req => book => book.id === parseInt(req.params.id);

// GET SINGLE BOOK

router.get('/:id', (req, res) => {

	if (req.params.id) {
		res.json(books.filter(idFilter(req)));
	} else {
		res.status(400).json({msg: `No book with the id of ${req.params.id}`})
	}
})

// ADD A BOOK

router.post('/', async (req, res) =>  {

	if (!val.validateInput(req.body.author) || !val.validateInput(req.body.title)) {
			let warning = "You have to write proper title/author name"
			res.render('add_a_book', {
				warning
			})
		}
//console.log(req.headers.referer)
// async req
	if (req.is('application/json')) {
		let newBook = {...req.body}
		let userId = req.session.passport.user
		newBook.user = userId
		try {
				let result = await db1.addToDatabase('books', newBook)
			res.send({ book_id: result.insertId })
		} catch(e) {console.log(e)}
//sync req
	} else {
		let newBook = {...req.body}
		if (!newBook.author || !newBook.title) {
			let warning = "You have to include title and author"
			res.render('add_a_book', {
				warning
			})
		} else {
			let userId = req.session.passport.user
			newBook.user = userId
			try {
				if (req.headers.referer.match(/book_list/)) {
					let result = await db1.addToDatabase('wishlistbook', newBook)
					res.redirect('back')
				} else {
					await db1.addToDatabase('books', newBook)
					res.redirect('/bookcase-view')
				}
			} catch(e) {console.log(e)}
		}
	}
});

// UPDATE A BOOK

router.post('/:id/update', async (req, res) => {
	const changedData = req.body
	if (req.params.id) {
		try {
			if (req.headers.referer.match(/book_list/)) {
				await db1.update('wishlistbook', changedData, req.params.id);
				res.redirect("back")
			} else {
				await db1.update('books', changedData, req.params.id);
				res.redirect("/bookcase-view")
			}
		} catch(e) {console.log(e)}
	} else {
		res.status(400).json({msg: `No book with the id of ${req.params.id}`})
	}
})

// DELETE A BOOK

router.post('/:id/delete', async (req, res) => {
	if (req.params.id) {
		try {
			if (req.headers.referer.match(/book_list/)) {
				await db.deleteBook('wishlistbook', req.params.id)
			} else {
				await db.deleteBook("books", req.params.id);
			}
			res.redirect('back');
		} catch(e) {console.log(e)}
	} else {
		res.status(400).json({msg: `No book with the id of ${req.params.id}`})
	}
})

// CLEAR BOOKS

router.post('/clear', async (req, res) => {

	await pool.query(`DELETE FROM wishlistbook WHERE user = ${req.user[0].id} AND status = "read"`)

})

module.exports = router;
