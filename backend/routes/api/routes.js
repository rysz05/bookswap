if (process.env.NODE_ENV !== 'production') {
	require('dotenv').config()
}

const express = require('express');
const passport = require('passport');
const db = require('../../modules/db.js')
const dbooks = require('../../modules/books.js')
const ch = require('../../modules/chat.js')
const router = express.Router();
const multer = require('multer');
const pool = require('../../database.js')

const storage = multer.diskStorage({
	destination: function(req, file, cb) {
		cb(null, './uploads/');
	},
	filename: function(req, file, cb) {
		//console.log(files)
		cb(null, file.originalname)
	}
})
const fileFilter = (req, file, cb) => {
	//reject a file
	if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
		cb(null, true);
	} else {
		cb(null, false);
	}
}
const upload = multer({
	storage: storage,
	limits: {
		fileSize: 1024*1024*5
	},
	fileFilter: fileFilter
});

const initializePassport = require('../../passport-config');

initializePassport()

router.get('/', checkAuthenticated, (req, res) => {
	res.render('index');
})

router.get('/hello', checkNotAuthenticated, (req, res) => {
  		res.render('hello')
})

router.get('/validateEmail', checkNotAuthenticated, (req, res) => {
  		res.render('validateEmail')
})

router.get('/findYourAccount', checkNotAuthenticated, (req, res) => {
  		res.render('findYourAccount')
})
router.get('/404', (req, res) => {
  		res.render('404')
})

router.get('/recoverCode', checkNotAuthenticated, (req, res) => {
	console.log('recoverCode')
	console.log(req)
/*	if(wrong_code) {
		let wrong_code = true
	}*/
	console.log(user_id)
  		res.render('recoverCode', {
  			user_id,
  			//wrong_code
  		})
})
router.get('/setNewPass', checkNotAuthenticated, (req, res) => {
	console.log(req)
	console.log('routes')
  		res.render('setNewPass', {
  			user_id
  		})
})

router.get('/login', checkNotAuthenticated, async (req, res) => {
	//let email = await req.consumeFlash('email')
	let noUser = await req.consumeFlash('noUser')
	let wrongPass = await req.consumeFlash('wrongPass')
	let notAuth = await req.consumeFlash('notAuth')
  res.render('login', {
  	//email,
  	notAuth,
  	noUser,
  	wrongPass

  })
})
router.get('/giveMoreInfo', checkAuthenticated, (req, res) => {
  res.render('giveMoreInfo')
})
router.get('/register', checkNotAuthenticated, async (req, res) => {
	let email = await req.consumeFlash('email')
	let username = await req.consumeFlash('username')
	let phone_nr = await req.consumeFlash('phone_nr')
	let userExists = await req.consumeFlash('userExists')

  res.render('register', {
  	email,
  	username,
  	phone_nr,
  	userExists
  })
});

router.get('/auth/facebook', passport.authenticate('facebook'));

router.get('/auth/facebook/callback', function(req, res, next) {
	passport.authenticate('facebook', async function(err, user) {
		if (err) {return next(err)}
		if (!user) {return res.redirect('/giveMoreInfo')}

		let x = await db.findItemBy('users', 'id', user.id)
		if (!x[0].city) {
			req.logIn(user, function(err) {
				if (err) {return next(err)}
				return res.redirect('/giveMoreInfo')
			})
		} else {
			req.logIn(user, function(err) {
				if (err) {return next(err); }
				return res.redirect('/')
			})
		}
	})(req, res, next)
})

router.get('/auth/google',
  passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/plus.login'] }));

router.get('/auth/google/callback', function(req, res, next) {
	passport.authenticate('google', async function(err, user) {
		if (err) {return next(err)}
		if (!user) {return res.redirect('/giveMoreInfo')}

		let x = await db.findItemBy('users', 'id', user.id)
		if (!x[0].city) {
			req.logIn(user, function(err) {
				if (err) {return next(err); }
				return res.redirect('/giveMoreInfo')
			})
		} else {
			req.logIn(user, function(err) {
				if (err) {return next(err); }
				return res.redirect('/')
			})
		}
	})(req, res, next)
})

router.post('/login', checkNotAuthenticated, passport.authenticate('local-login', {
	  successRedirect: '/',
	  failureRedirect: '/login',
	  failureFlash: false
	}
))

router.post('/register', upload.any(), checkNotAuthenticated, passport.authenticate('local-register', {
	  successRedirect: '/validateEmail',
	  failureRedirect: '/register',
	  failureFlash: false
	}
))
/*router.get('/messages',  async (req, res) => {
	res.cookie('user', req.session.passport.user)
	if (!req.query.otherUser_id) {
		let data = await chat.ifNoChat(req, req.session.passport.user, res, 'messages')
		// IF NO CHAT DISPLAYED DISPLAY ALL CHATS FROM THE LOGGED USER
	} else {
		let data = await chat.thereIsChat(req, req.session.passport.user, res, 'messages')
		// SITE ACCESSED WITH ANOTHER USER ID, GET THE CHAT
	}
})*/

router.get('/socket', checkAuthenticated, async (req, res) => {
	let user_id = req.session.passport.user
	res.cookie('user', user_id)

	function isBookReserved(book) {
		if (book.status == 'reserved') {
						res.cookie('book_reserved', true)
					} else {
						res.cookie('book_reserved', false)
					}
	}

	if (!req.query.otherUser_id) {
		// IF NO OTHER USER_ID DISPLAYED DISPLAY ALL CHATS FROM THE LOGGED USER
		try {
			let user_chats = await db.getUserChats(user_id)
			if (user_chats.length) {
				//console.log(user_chats[0].id)
				res.cookie('chatID', user_chats[0].id)
				let result = await ch.renderChatView(user_id, user_chats[0])
				let chatID = user_chats[0].id
				let {book, renderCheckbox, my_asks, not_my_asks, messages, personalData, userData
				} = result
				db.isBookReserved(res, book)

				res.render ('socket', {
						renderCheckbox,
						my_asks,
						not_my_asks,
						messages,
						book,
						personalData,
						chatID,
						userData
					})
			} else {
				let no_results = true
				res.render ('socket', {
					no_results,
				})
			}
		} catch (e) {console.log(e)}
	} else {
		// SITE ACCESSED WITH ANOTHER USER ID, GET THE CHAT

		let chat = await db.getChat(user_id, req.query.otherUser_id)
		console.log(chat)
		// CHECK IF CHAT WITH THIS USER ALREADY EXISTS
		console.log(chat.length)
		if (chat.length) {

			let chatID = chat[0].id
			if (chat[0].book_id !== req.query.book_id && req.query.book_id) {
				let changedData = {book_id: req.query.book_id}
				try {
					await db.update('chats', changedData, chatID)
				} catch (e) {console.log(e)}
			}
			//console.log(chatID)
			res.cookie('chatID', chatID)
			try {
				let result = await ch.renderChatView(user_id, chat[0])
				let {book, personalData, my_asks, not_my_asks, messages, renderCheckbox, userData} = result
				db.isBookReserved(res, book)

				res.render('socket', {
				renderCheckbox,
				my_asks,
				not_my_asks,
				personalData,
				book,
				chatID,
				messages,
				userData
			})
			} catch (e) {console.log(e)}
		} else {	// CREATE A NEW CHAT
			try {
				const newChat = {
					'first_user' : user_id,
					'second_user' : req.query.otherUser_id,
					'book_id' : req.query.book_id
				}
				let  result = await db.addToDatabase('chats', newChat)
				newChat.id = result.insertId
				let chatID = newChat.id
				//console.log(chatID)
				res.cookie('chatID', chatID)

				let data = await ch.renderChatView(user_id, newChat)
				let {book, renderCheckbox, my_asks, not_my_asks, personalData, messages, userData} = data
				db.isBookReserved(res, book)

				res.render('socket', {
					book,
					renderCheckbox,
					my_asks,
					not_my_asks,
					personalData,
					chatID, // gives the id for displaying book name
					messages,
					userData
				})
			} catch (e) {console.log(e)}
		}
	}
})

router.get('/add_a_book', checkAuthenticated, async (req, res) => {
		res.render('add_a_book', {
		})
	});

router.get('/terms', async (req, res) => {
		res.render('terms', {
		})
	});

router.get('/giveMoreBookInfo', checkAuthenticated, (req, res) => {
	let book_id = req.query.book_id

	res.render('giveMoreBookInfo', {
		book_id
	})
});

router.get('/bookcase-list_view.html', checkAuthenticated, async (req, res) => {
try {
		let freeBooks = [];
		let reservedBooks = []
		let books = await db.selectAllUsersBooks(req.session.passport.user)
		books.forEach((el) => {
			if (el.status == "free") {
				freeBooks.push(el)
			} else {
				reservedBooks.push(el)
			}
		})
		res.render('list-view', {
			title: "Add a book",
			freeBooks,
			reservedBooks
		})
	} catch(e) {console.log(e)}
});

router.get('/bookcase-view', checkAuthenticated, async (req, res) => {
	try {
		let freeBooks = [];
		let reservedBooks = []
		let books = await db.selectAllUsersBooks(req.session.passport.user)
		books.forEach((el) => {
			if (el.status == "free") {
				freeBooks.push(el)
			} else {
				reservedBooks.push(el)
			}
		})
		res.render('bookcase-view', {
			title: "Add a book",
			freeBooks,
			reservedBooks
		})
	} catch(e) {console.log(e)}
});


router.get('/search', checkAuthenticated, async (req, res) => {
	res.cookie('user', req.session.passport.user)
	try {
	let books = await db.allTitlesAndAuthorsArray();
  		res.render('search', {
  			books
  		})
  	} catch(e) {console.log(e)}
})

router.get('/scan', checkAuthenticated, async (req, res) => {
  		res.render('scan')
})


router.get('/book_list', checkAuthenticated, async (req, res) => {
	let toBeRead = await dbooks.getBooksByIdAndStatus(req.session.passport.user, "toBeRead")
	let read = await dbooks.getBooksByIdAndStatus(req.session.passport.user, "read")
	//console.log(books)
  		res.render('bookWishList', {
  			toBeRead,
  			read
  		})
})

router.get('/menu', checkAuthenticated, async (req, res) => {

	try {
		let userInfo = await db.getUserInfo(req.session.passport.user)

		let data = userInfo[0]
		let chatsNr = await db.howManychats(req.session.passport.user)
		chatsNr = chatsNr[0].chatsNr
		let booksNr = await db.howManyBooks(req.session.passport.user)
		booksNr = booksNr[0].books_nr
		let friends = await db.getAllChattersNames(req.session.passport.user)
		/*let userChats = await db.getUserChats(req.session.passport.user)
		console.log(userChats)*/
		res.render('menu', {
			data,
			chatsNr,
			booksNr,
			friends
		})
	} catch(e) {console.log(e)}
});



router.get('/logout', (req, res) => {
  req.session = null;
  res.redirect('/hello');
});

function checkAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next()
  } else
  res.redirect('/hello')
  return next()
}

async function checkNotAuthenticated(req, res, next) {
	if (req.user) {
		let result = await pool.query(`SELECT authStatus FROM users WHERE id = ${req.user[0].id}`)
		if (req.isAuthenticated() && result.authStatus === "validated") {
	    	return res.redirect('/')
	  	} else next()
	}
	 else {
		if (req.isAuthenticated()) {
	    	return res.redirect('/')
	  	} else next()
	}
}


module.exports = router;

