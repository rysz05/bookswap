const express = require('express');
const router = express.Router();
const pool = require('../../database.js')
const db = require('../../ts-build/modules/db.js')
const val = require('../../ts-build/modules/validation.js')
const geo = require('../../ts-build/modules/distance.js')

router.post('/search', async (req, res) => {

// check if correct input
    if (!val.validateInput(req.body.ask)) {
            let books = await db.allBooksArray()
            let no_results = true
            res.render('search', {
                    no_results,
                    books
                })
        }
    let userID = req.session.passport.user
    let newSearch = {...req.body};
    try {
        //console.log(newSearch)
        let searchResult = await db.searchBooks(newSearch, userID);
        if (searchResult.length) {
//let radius = newSearch.distance
            let radius = 21200
// display only books in the chosen radius
            let displayedResult = await geo.booksToDisplay(searchResult, radius, userID)
            //console.log(displayedResult)
// for autocomplete:
            let books = await db.allTitlesAndAuthorsArray()
//console.log(books)
            res.render('search', {
                    displayedResult,
                    books
                })
        } else {
// for autocomplete:
            let books = await db.allTitlesAndAuthorsArray()
            let no_results = true
            res.render('search', {
                    no_results,
                    books
                })
        }
    } catch (e) {console.log(e)}
})

router.post('/search/random', async (req, res) => {
    try {
        //let books = await db.selectAll('books');
        let books = await db.allBooksArray()
        if (books.length) {
            let min = 0;
            let max = books.length - 1;
            let randomnumber = Math.floor(Math.random() * (max - min) + min);
            let displayedResult = await pool.query(`SELECT author, title, genre, img_path, state, quote, book_condition, status, user, name, lat, lng, books.id AS book_id FROM books INNER JOIN users ON users.id = books.user AND NOT user = '${req.user}' AND NOT status = 'reserved' LIMIT ${randomnumber-1},1`)
            res.render('search', {
                    displayedResult,
                    books
                })
        } else {
            let no_results = true
            res.render('search', {
                    no_results,
                    books
                })
        }
    } catch (e) {console.log(e)}
})

module.exports = router;
