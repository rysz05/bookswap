const res = require("../modules/validation.js")

test('Validate email', () => {
		const email = 'bum@op.pl';
		expect(res.validateEmail(email)).toBe(true);
	})

test('Validate phone nr', () => {
		const phone_nr = '555 225 458';
		expect(res.validatePhoneNr(phone_nr)).toBe(true);
	})

test('Validate username', () => {
		const username = 'MAdzia87';
		expect(res.validateUsername(username)).toBe(true);
	})

test('Validate input', () => {
		const input = 'MAdzia';
		expect(res.validateInput(input)).toBe(true);

	})
