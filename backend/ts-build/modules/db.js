"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const pool = require('../database.js');
/**
Updates a table by an id
@param {string} database - the table to be updated
@param {Object} changedData - the object to be add to table
@param {number} id - id where the data is being changed
@returns {Object} The response object
*/
function update(database, changedData, id) {
    return __awaiter(this, void 0, void 0, function* () {
        let result = yield pool.query(`UPDATE ${database} SET ? WHERE id = ${id}`, changedData);
        //console.log(`UPDATE ${database} SET updated_at = CURRENT_TIMESTAMP WHERE id = ${id}`)
        console.log(database);
        if (database != "wishlistbook") {
            pool.query(`UPDATE ${database} SET updated_at = CURRENT_TIMESTAMP WHERE id = ${id}`);
        }
        return result;
    });
}
/**
Get the chat from users ids
@param {number} user_1_id - the first users id
@param {number} user_2_id - the second users id
@returns {Object} The response object
*/
function getChat(user_1_id, user_2_id) {
    return __awaiter(this, void 0, void 0, function* () {
        let result = yield pool.query(`SELECT * FROM chats WHERE first_user IN (${user_1_id}, ${user_2_id}) AND second_user IN (${user_1_id}, ${user_2_id})`);
        return result;
    });
}
/**
Display users chat list using his id
@param {number} user_id - the users id
@returns {Object} The response object of all users chats
*/
function getUserChats(user_id) {
    return __awaiter(this, void 0, void 0, function* () {
        let result = yield pool.query(`SELECT * FROM chats WHERE first_user = ${user_id} OR second_user = ${user_id}`);
        return result;
    });
}
/**
Set the other_user variable for chats list to get hyperlinks
@param {number} thisUser_id - the logged in users id
@param {number} chat_id - the chat nr between the two users
@returns {number} the id of the other user taking part in the chat
*/
function setOther(thisUser_id, chat_id) {
    return __awaiter(this, void 0, void 0, function* () {
        let other;
        let chat = yield pool.query(`SELECT * FROM chats WHERE id = ${chat_id}`);
        if (chat[0].first_user == thisUser_id) {
            other = chat[0].second_user;
        }
        else {
            other = chat[0].first_user;
        }
        return other;
    });
}
/**
 * Gets the names af people the user has chats with
 * @param  {number} user_id
 * @return {Object} two arrays of users divided by who inicitated the chat
 */
function getAllChattersNames(user_id) {
    return __awaiter(this, void 0, void 0, function* () {
        let res = yield pool.query(`
(SELECT name, first_user, second_user, chats.id FROM users inner JOIN chats ON users.id = chats.second_user WHERE first_user = ${user_id}) union
(SELECT name, first_user, second_user, chats.id FROM users inner JOIN chats ON users.id = chats.first_user WHERE second_user = ${user_id})`);
        /*my_asks = res.filter((el) => el.first_user === user_id)
        not_my_asks = res.filter((el) => el.second_user === user_id)*/
        return res;
    });
}
/**
Position the messages on right or left depending on user id
@param {Object} data - all the chats messages
@param {number} user_id - the logged in users id
@returns {Object} all the chats messages with a new parameter
*/
function setActiveUser(data, user_id) {
    let messages = data;
    for (let i = 0; i < messages.length; i++) {
        if (messages[i].user == user_id) {
            messages[i].activeUser = true;
        }
    }
    return messages;
}
/**
Produces an array of book titles and authors for the autocomplete
@returns {Array} array of each title and author name from the books table
*/
function allTitlesAndAuthorsArray() {
    return __awaiter(this, void 0, void 0, function* () {
        let data = yield pool.query(`SELECT title, author FROM books`);
        let books = [];
        for (var i = 0; i < data.length; i++) {
            if (!(books.includes(data[i].author))) {
                books.push(data[i].author);
            }
            if (!(books.includes(data[i].title))) {
                books.push(data[i].title);
            }
        }
        return books;
    });
}
/**
Produces an array of book titles
@returns {Array} array of each title from the books table
*/
function allBooksArray() {
    return __awaiter(this, void 0, void 0, function* () {
        let data = yield pool.query(`SELECT title FROM books`);
        /*	let books = [];
                    for (var i = 0; i <data.length; i++) {
                        if (!(books.includes(data[i].author))) {
                            books.push(data[i].author);
                        }
                        if (!(books.includes(data[i].title))) {
                            books.push(data[i].title)
                        }
                    }*/
        return data;
    });
}
// books search feature
/**
Produces an array of book titles and authors for the autocomplete
@param {Object} input - the form input object
@param {number} user_id - the logged in users id
@returns {Object} object of all the requested data
*/
function searchBooks(input, user_id) {
    return __awaiter(this, void 0, void 0, function* () {
        let data = yield pool.query(`SELECT author, title, genre, img_path, state, quote, book_condition, status, user, name, lat, lng, books.id AS book_id FROM books INNER JOIN users ON users.id = books.user WHERE (Author = '${input.ask}' OR title = '${input.ask}' OR genre LIKE '%${input.genre}%') AND NOT user = '${user_id}' AND NOT status = 'reserved'`);
        return data;
    });
}
/**
Gets users personall data from the users table
@param {number} user_id - the logged in users id
@returns {Object} The response object
*/
function getPersonalData(user_id) {
    return __awaiter(this, void 0, void 0, function* () {
        let data = yield pool.query(`SELECT name, email, img_path FROM users WHERE id = ${user_id}`);
        return data;
    });
}
/**
Display or not the reserve book checkbox
@param {number} first_user_id - the logged in users id
@param {number} user_id - the logged in users id
@returns {boolean} decides if to render the checkbox for reserving the box
*/
function renderBookCheckbox(first_user_id, user_id) {
    let renderBookcheckbox;
    if (first_user_id == user_id) {
        renderBookcheckbox = false;
    }
    else {
        renderBookcheckbox = true;
    }
    return renderBookcheckbox;
}
/**
Get proper data in chat list
@param {string} whichUser - the id of the book to delete
@param {number} currentUser - the logged in users id
@returns {Object} The response object

async function displayChatList(whichUser, currentUser) {
    let res = await pool.query(`SELECT books.id AS book_id, chats.id AS chat_id, first_user, second_user, books.user FROM chats INNER JOIN users ON users.id = chats.${whichUser} INNER JOIN books ON users.id = books.user WHERE ${whichUser} = ${currentUser}`)
    return res
}
*/
/**
Get info for menu page
@param {number} user_id - the logged in users id
@returns {Object} The response object
*/
function getUserInfo(user_id) {
    return __awaiter(this, void 0, void 0, function* () {
        let res = yield pool.query(`SELECT users.id, name, email, img_path, phone_nr, img_path, city, street, building_nr, rating FROM users WHERE users.id = '${user_id}'`);
        return res;
    });
}
/*async function getChatInfo(user_id) {
    let res = await pool.query(`SELECT users.id, COUNT(chats.id) AS all_chats, first_user, second_user FROM users INNER JOIN chats ON users.id = chats.first_user WHERE chats.first_user = '${user_id}' OR chats.second_user = '${user_id}'`)
    return res
}*/
/**
Get the number of chats the a user has
@param {number} user_id - the logged in users id
@returns {number} The number of chats a user has
*/
const howManychats = (user_id) => __awaiter(void 0, void 0, void 0, function* () {
    let data = yield pool.query(`SELECT COUNT(chats.id) AS chatsNr FROM chats WHERE first_user = '${user_id}' OR second_user = '${user_id}'`);
    return data;
});
/**
Get the number of books the a user has
@param {number} user_id - the logged in users id
@returns {number} The number of books a user has
*/
const howManyBooks = (user_id) => __awaiter(void 0, void 0, void 0, function* () {
    let data = yield pool.query(`SELECT COUNT(books.id) AS books_nr FROM books WHERE user = '${user_id}'`);
    return data;
});
// SECOND LEVEL
/**
Find a user by a parameter
@param {string} byWhat - the column name that you want to search by
@param {*} par - the data you have to search by
@returns {Object} The response object
*/
const findUsersBy = (byWhat, par) => __awaiter(void 0, void 0, void 0, function* () {
    let data = yield findItemBy('users', byWhat, par);
    return data;
});
/**
Selects all of one chats messages and orders the by date ascending
@param {number} chat_id - the chats id
@returns {Object} The response object
*/
function selectAllById(chat_id) {
    return __awaiter(this, void 0, void 0, function* () {
        let result = yield selectAllOrderbyDateASC('messages', 'chat_id', chat_id);
        return result;
    });
}
/**
Selects all of the users books
@param {number} user_id - the logged users id
@returns {Object} The response object
*/
function selectAllUsersBooks(user_id) {
    return __awaiter(this, void 0, void 0, function* () {
        let result = yield pool.query(`SELECT * FROM books WHERE user = ${user_id}`);
        return result;
    });
}
// MAIN FUNCTIONS
/**
Selects all of values from one column from a picked table and order ascending
@param {string} database - the table to search through
@param {string} byWhat - the column to get the data from
@param {*} what - the parameter
@returns {Object} The response object
*/
function selectAllOrderbyDateASC(database, byWhat, what) {
    return __awaiter(this, void 0, void 0, function* () {
        if (typeof database == "string" && typeof byWhat == "string") {
            let result = yield pool.query(`SELECT * FROM ${database} WHERE ${byWhat} = '${what}' ORDER BY created_at ASC`);
            return result;
        }
    });
}
/*async function selectAll(database){
    result = await pool.query(`SELECT * FROM ${database}`);
    return result
}*/
/**
Selects all ids from a picked table
@param {string} database - the table to search through
@returns {Object} The response object
*/
function selectAllIds(database) {
    return __awaiter(this, void 0, void 0, function* () {
        let result = yield pool.query(`SELECT id FROM ${database}`);
        return result;
    });
}
/**
Selects all of values from one column from a picked table
@param {string} database - the table to search through
@param {string} byWhat - the column to get the data from
@param {*} what - the parameter
@returns {Object} The response object
*/
function findItemBy(database, byWhat, what) {
    return __awaiter(this, void 0, void 0, function* () {
        let data = yield pool.query(`SELECT * FROM ${database} WHERE ${byWhat} = '${what}'`);
        return data;
    });
}
/**
Adds a new item to the table
@param {string} database - the table to search through
@param {Object} newItem - a random object
@returns {Object} The response object
*/
function addToDatabase(database, newItem) {
    return __awaiter(this, void 0, void 0, function* () {
        let data = yield pool.query(`INSERT INTO ${database} SET ?`, newItem);
        //console.log(data.insertId)
        if (database != "wishlistbook") {
            pool.query(`UPDATE ${database} SET created_at = CURRENT_TIMESTAMP WHERE id = ${data.insertId}`);
        }
        return data;
    });
}
function isBookReserved(res, book) {
    if (book.status == 'reserved') {
        res.cookie('book_reserved', true);
    }
    else {
        res.cookie('book_reserved', false);
    }
}
module.exports = {
    findUsersBy,
    findItemBy,
    addToDatabase,
    //selectAll,
    selectAllById,
    update,
    getChat,
    getUserChats,
    setActiveUser,
    allBooksArray,
    searchBooks,
    selectAllUsersBooks,
    getPersonalData,
    setOther,
    //displayChatList,
    //getChatInfo,
    getUserInfo,
    howManychats,
    howManyBooks,
    renderBookCheckbox,
    selectAllIds,
    allTitlesAndAuthorsArray,
    getAllChattersNames,
    isBookReserved,
};
//# sourceMappingURL=db.js.map