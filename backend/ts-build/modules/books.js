"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const pool = require('../database.js');
/**
Adds a new book into the books table
@param {Object} newBook - the new book to add
@returns {Object} The response object
*/
function addBook(newBook) {
    return __awaiter(this, void 0, void 0, function* () {
        result = pool.query(`INSERT INTO books SET ?`, newBook);
        return result;
    });
}
/**
Updates a book from the books table by id
@param {object} changedData - the changed parameters
@param {number} id - the id of the book to change
@returns {Object} The response object
*/
function updateBook(changedData, id) {
    return __awaiter(this, void 0, void 0, function* () {
        result = pool.query(`UPDATE books SET ? WHERE id = ${id}`, changedData);
        return result;
    });
}
/**
Delete a book from the books table by id
@param {number} id - the id of the book to delete
@returns {Object} The response object
*/
function deleteBook(database, id) {
    return __awaiter(this, void 0, void 0, function* () {
        result = pool.query(`DELETE FROM ${database} WHERE id = ${id}`);
        return result;
    });
}
function getBooksByIdAndStatus(user_id, status) {
    return __awaiter(this, void 0, void 0, function* () {
        data = yield pool.query(`SELECT * FROM wishlistbook WHERE user = "${user_id}" AND status = "${status}"`);
        return data;
    });
}
module.exports = {
    addBook,
    updateBook,
    deleteBook,
    getBooksByIdAndStatus
};
//# sourceMappingURL=books.js.map