"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
Validates emails
@param {string} email - user's input
@returns {boolean} is it a good email
*/
function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
/**
Validates phone nr
@param {string} phone_nr - user's input
@returns {boolean} is it a good phone number
*/
function validatePhoneNr(phone_nr) {
    const re = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
    return re.test(phone_nr);
}
/**
Validates username
@param {string} username - user's input
@returns {boolean} is it a good username
*/
function validateUsername(username) {
    const re = /^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/;
    return re.test(username);
}
/**
Validates input
@param {string} email - user's input
@returns {boolean} is it a good input
*/
function validateInput(input) {
    const re = /^[a-zA-Z\s]*$/;
    return re.test(input);
}
module.exports = {
    validateEmail,
    validatePhoneNr,
    validateUsername,
    validateInput
};
//# sourceMappingURL=validation.js.map