"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const db = require('./db.js');
/**
Renders the chat page
@param {number} user_id - the logged user id
@param {Object} chat - the accessed chat object
@returns {Object} all the objects needed to load the page properly
*/
function renderChatView(user_id, chat) {
    return __awaiter(this, void 0, void 0, function* () {
        //let chat = await db.findItemBy('chats', 'id', chat.id)
        // shows all messages from this chat
        let data = yield db.selectAllById(chat.id);
        let messages = db.setActiveUser(data, user_id);
        let other = yield db.setOther(user_id, chat.id);
        // get all chats
        //let allChats = await db.getAllChats()
        let userData = yield db.getPersonalData(user_id);
        userData = userData[0];
        // show list of chats
        let asks = yield db.getAllChattersNames(user_id);
        my_asks = asks.filter((el) => el.first_user === user_id);
        not_my_asks = asks.filter((el) => el.second_user === user_id);
        // shows other users name
        let personalData = yield db.getPersonalData(other);
        personalData = personalData[0];
        let book = yield db.findItemBy('books', 'id', chat.book_id);
        book = book[0];
        let renderCheckbox = db.renderBookCheckbox(chat.first_user, user_id);
        return {
            userData,
            personalData,
            my_asks,
            not_my_asks,
            messages,
            renderCheckbox,
            book
        };
    });
}
module.exports = {
    renderChatView
};
//# sourceMappingURL=chat.js.map