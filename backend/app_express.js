var cookieSession = require('cookie-session')
const express = require('express');
const exphbs = require('express-handlebars');
var http = require('http')
const app = express();
const path = require('path')
var cookieParser = require('cookie-parser')
var server = http.createServer(app)
const io = require('socket.io')(server)
const bodyParser = require('body-parser');
const passport = require('passport');
const session = require('express-session')
const formatMessage = require('./utils/messages');
const {userJoin, getCurrentUser, userLeave} = require('./utils/chatRoom');
const { flash } = require('express-flash-message');
require('dotenv').config()

const Sentry = require("@sentry/node");
const Tracing = require("@sentry/tracing");
Sentry.init({
  dsn: "https://e0dbe3aac84a47aeb00f34dd7cf4d5f4@o512655.ingest.sentry.io/5613248",
  tracesSampleRate: 1.0,
});

io.on('connection', socket => {
	socket.on('joinRoom', ({ user_id, chat_id }) => {
	    const user = userJoin(socket.id, user_id, chat_id);
	    socket.join(user.chat_id);
	});

	  // Listen for chatMessage
	socket.on('chatMessage', msg => {
	  	const user = getCurrentUser(socket.id);
	    io.to(user.chat_id).emit('message', formatMessage(msg));
	});

	socket.on('disconnecting', () => {
		const user = userLeave(socket.id);
	});
})

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static('public'));
app.use(express.static(path.resolve(__dirname, 'dist')))
app.use('/uploads', express.static('uploads'));

app.use(cookieSession({
	name: 'session',
	keys: ['bla bla'],
	maxAge: 24 * 60 *60 *1000
}))

app.use(session({
	secret: "random text",
	resave: false,
	saveUninitialized: true
}));
app.use(flash({sessionKeyName: 'flashMessage', useCookieSession: true}));

app.use(cookieParser())
app.use(passport.initialize());
app.use(passport.session());

let routes = [
	{'path': '/','name':'routes'},
	{'path': '/api/books','name':'books'},
	{'path': '/','name':'chat'},
	{'path': '/','name':'search'},
	{'path': '/', 'name': "users"}
]

routes.forEach((r) => app.use(r.path, require(`./routes/api/${r.name}.js`)))

const PORT = process.env.PORT /*|| 5000*/;
server.listen(PORT, () => console.log(`Server started on port ${PORT}`));
