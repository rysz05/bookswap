var path = require('path');

module.exports = {
  mode: 'development',
  entry:
  {
    main: [
    './public/js/bookCondPic.js',
    './public/js/checkRadioCont.js',
    './public/js/colors.js',
    './public/js/modal.js',
    './public/js/validatePassword.js',
    './public/js/rememberMe.js',
    //'./public/js/bookWishList.js',
    './public/css/add_a_book.css',
    './public/css/bookcase-list-view.css',
    './public/css/bookcase.css',
    './public/css/bookswap_maincss.css',
    './public/css/mainpage.css',
    './public/css/modal.css',
    './public/css/radio-container.css',
    './public/css/start.css',
    './public/css/chatPage.css',
    './public/css/autocomplete.css',
    './public/css/hello.css',
    './public/css/bookWishList.css'
    ],
    chat: [
    './public/js/sockets-index.js',
    './public/js/chatScrolldown.js',

    ],
    search: [
    './public/js/autocomp.js',
    './public/js/searchSite.js',

    ],
    scanner: './public/js/barcodeScanner.js',
  },
  devtool: false,
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].bundle.js',
   // publicPath: '/dist'
  },
  mode: 'production',
    module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
        exclude: [
          path.resolve(__dirname, "node_modules")
        ],
      },
      {
        test: /\.svg$/,
        loader: 'url-loader'
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
        use: [
	        {
	        	loader: 'file-loader',
	        	options: {
	        		name: '[name].[ext]'
	        	}
	        }
        ]
      },
      {
        test: /\.html$/i,
        loader: 'html-loader',
      }
    ],
  },
};

