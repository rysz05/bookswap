const LocalStrategy = require('passport-local').Strategy
const bcrypt = require('bcrypt');
const passport = require('passport');
const pool = require('./database.js')
const db = require('./modules/db.js')
const val = require('./modules/validation.js')
const geo = require('./ts-build/modules/distance.js')
const configAuth = require('./auth.js')
const mailjet = require('./modules/mailjet.js')
const { v4: uuidv4 } = require('uuid');
const moment = require('moment');

	passport.serializeUser((user, done) => {
		done (null, user.id)
	})

	passport.deserializeUser(async (user, done) => {
		try {
			let userToDeserialize = await pool.query(`SELECT * FROM users WHERE id = ${user}`);
			if(!userToDeserialize) {
				return done(new Error('user not found'));
			} done(null, userToDeserialize);
		} catch (e) {
			done(e);
		}
	 })

function initialize() {

	const authenticateUserRegister = async (req, email, password, done) => {
		//console.log(req.files)
		if (!val.validateEmail(email)) {
			await req.flash('email', 'invalid email format')
			return done(null, false/*, {message: "invalid email format"}*/)
		}
		if (!val.validateUsername(req.body.name)) {
			//console.log('bad username format')
			await req.flash('username', 'Choose a username with 8 to 20 characters')
			return done(null, false/*, {message: "Write a username with 8 to 20 characters"}*/)
		}
		if (!val.validatePhoneNr(req.body.phone_nr)) {
			await req.flash('phone_nr', 'invalid phone number format')
			//console.log('bad phone nr format', {message: "invalid phone number format"})
			return done(null, false)
		}

		try {
			let user = await db.findUsersBy('email', email)

			if (user.length) {
				await req.flash('userExists', 'This email was already used')
				return done(null, false)
			}} catch (e) {return done(e)}
		const newUser = {...req.body}
		const hashedPassword = await bcrypt.hash(password, 10)

		newUser.password = hashedPassword;
		if(req.files) {
			newUser.img_path = req.files[0].path
		}
		let address = `${newUser.street} ${newUser.building_nr}, ${newUser.city}`
		try {
			let coordinates = await geo.forwardGeocoding(address)
			//console.log(coordinates)
			if(coordinates) {
				newUser.lat = coordinates.lat
				newUser.lng = coordinates.lng
			} else {
				console.log(newUser)
				console.log(coordinates)
			}
		} catch(e) {return done(e)}
		delete newUser.r_password
		try {
			let token = uuidv4()
			newUser.authToken = token
			let add = await db.addToDatabase('users', newUser)
			//pool.query(`UPDATE users SET created_at = CURRENT_TIMESTAMP WHERE id = ${newUser.id}`)
			if (add) {
			newUser.id = add.insertId;
			mailjet.regValidationEmail(newUser.name, newUser.email, token, newUser.id)
			return done(null, newUser);
			}
		} catch(e) {
			return done(e)
		}
		return done(null, false)
	}

	passport.use('local-register', new LocalStrategy({
		usernameField: 'email',
		passwordField: 'password',
		passReqToCallback: true
	},
	 authenticateUserRegister))


    const authenticateUserLogin = async (req, email, password, done) => {
    	if (!val.validateEmail(email)) {
    		//await req.flash('email', 'invalid email format')
			return done(null, false)
		}
    	try {
	        let user = await db.findUsersBy('email', email)

	        if (user[0].authStatus !== "validated") {
	        	await req.flash('notAuth', "Verify your account by clicking on the link we send to Your email")
				return done(null, false)
	        } else if (!user.length) {
				await req.flash('noUser', "There is no user under this email")
				return done(null, false)
			} else {
				try {
				let match = await bcrypt.compare(password, user[0].password);
				if (match) {
					//console.log("user: "+user[0].id+" logged in")
					return done(null, user[0]); // create the loginMessage and save it to session as flashdata
				} else {
					await req.flash('wrongPass', "Wrong password")
					return done(null, false)
					}
				} catch(e) {
						return done(e)
					}
			}
		} catch(e) {
			return done(e)
		}
	}

	passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    authenticateUserLogin))

	const authenticate = (token) => async (accessToken, refreshToken, profile, done) => {
		try {
			let user = await db.findUsersBy(`${token}`, profile.id )
			//console.log(user)
			if (user.length) {
				//console.log('user already exists')
				return done(null, user[0])
			} else {
				const newUser = {}
				newUser.name = profile.displayName
				newUser[`${token}`] = profile.id;
				newUser.authStatus = "validated"
				await db.addToDatabase('users', newUser)
				let time = moment().format('MMMM Do YYYY, h:mm:ss a')
				mailjet.newUserMailNotification(newUser.name, time)
				//pool.query(`UPDATE users SET created_at = CURRENT_TIMESTAMP WHERE id = ${newUser.id}`)
				done(null, newUser.id);
			}
		} catch(e) {done(e)}
	}

	let FacebookStrategy = require('passport-facebook').Strategy;
		passport.use(new FacebookStrategy({
		    clientID: configAuth.facebookAuth.clientID,
		    clientSecret: configAuth.facebookAuth.clientSecret,
		    callbackURL: configAuth.facebookAuth.callbackURL
		  }, authenticate('accessTokenFB')));

	let GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
		passport.use(new GoogleStrategy({
	    clientID: configAuth.googleAuth.clientID,
	    clientSecret: configAuth.googleAuth.clientSecret,
	    callbackURL: configAuth.googleAuth.callbackURL
	  }, authenticate('accessTokenGoogle')));

}
module.exports = initialize
