//const axios = require('axios').default;
function scanBarcode() {

    var App = {
        init: function() {
            App.attachListeners();
        },
        attachListeners: function() {
            var self = this;
            let input = document.querySelector(".controls input[type=file]")
            if (input) {
                input.addEventListener('change', (e) => {
                    if (e.target.files && e.target.files.length) {
                        App.decode(URL.createObjectURL(e.target.files[0]));
                    }
                })

            }
        },
        decode: function(src) {
            var self = this;
            self.state.src = src
            config = self.state

            Quagga.decodeSingle(config, function(result) {
                if (result) {
                    let text = "Detected"
                    document.getElementById('error-info').innerHTML = text
                } else {
                    let text = "Not detected, try with another picture"
                    document.getElementById('error-info').innerHTML = text
                }
            });
        },

        state: {
            inputStream: {
                size: 800,
                singleChannel: false
            },
            locator: {
                patchSize: "large",
                halfSample: false
            },
            decoder: {
                readers: [{
                    format: "ean_reader",
                    config: {}
                }]
            },
            locate: true,
            src: null
        }
    };

    App.init();

    function calculateRectFromArea(canvas, area) {
        var canvasWidth = canvas.width,
            canvasHeight = canvas.height,
            top = parseInt(area.top)/100,
            right = parseInt(area.right)/100,
            bottom = parseInt(area.bottom)/100,
            left = parseInt(area.left)/100;

        top *= canvasHeight;
        right = canvasWidth - canvasWidth*right;
        bottom = canvasHeight - canvasHeight*bottom;
        left *= canvasWidth;

        return {
            x: left,
            y: top,
            width: right - left,
            height: bottom - top
        };
    }

    Quagga.onProcessed(function(result) {
        var drawingCtx = Quagga.canvas.ctx.overlay,
            drawingCanvas = Quagga.canvas.dom.overlay,
            area;

        if (result) {
            if (result.boxes) {
                drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
                result.boxes.filter(function (box) {
                    return box !== result.box;
                }).forEach(function (box) {
                    Quagga.ImageDebug.drawPath(box, {x: 0, y: 1}, drawingCtx, {color: "green", lineWidth: 2});
                });
            }

            if (result.box) {
                Quagga.ImageDebug.drawPath(result.box, {x: 0, y: 1}, drawingCtx, {color: "#00F", lineWidth: 2});
            }

            if (result.codeResult && result.codeResult.code) {
                Quagga.ImageDebug.drawPath(result.line, {x: 'x', y: 'y'}, drawingCtx, {color: 'red', lineWidth: 3});
            }

            if (App.state.inputStream.area) {
                area = calculateRectFromArea(drawingCanvas, App.state.inputStream.area);
                drawingCtx.strokeStyle = "#0F0";
                drawingCtx.strokeRect(area.x, area.y, area.width, area.height);
            }
    }
    });

    Quagga.onDetected(async function(result) {
        var code = result.codeResult.code,

            canvas = Quagga.canvas.dom.image;

            let headers = {
                "Content-Type": 'application/x-www-form-urlencoded; charset=UTF-8',
            }
            fetch(`https://openlibrary.org/isbn/${code}.json`, {headers: headers})
                .then(response => {
                    return response.json()
                })
                .then(json => {
                    let data = {};
                    if (json.subjects) {
                        data.genre = json.subjects[1]
                    }
                    data.title = json.title;
                    if (json.by_statement) {
                        data.author = json.by_statement.split(" ").slice(0,2).join(" ")
                    } else {
                        data.author = "Fix it later"
                    }
                    fetch('/api/books', {
                            method: 'POST',
                            body: JSON.stringify(data),
                            headers: {
                                "Content-type": "application/json; charset=UTF-8"
                            },
                        })
                    .then(function(response) {
                      response.json().then(data => {

                        window.location = `/giveMoreBookInfo?book_id=${data.book_id}`
                      })
                    })

                .catch(error => {
                    console.log('Error:', error)
                });
            })

    });
}
scanBarcode()
