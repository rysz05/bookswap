
let allChatsBtn = document.getElementById('all-chats')
let myAsksBtn = document.getElementById('my_asks-btn')
let notMyAsksBtn = document.getElementById('not_my_asks-btn')

let my_asks = document.querySelector('.my_asks')
let not_my_asks = document.querySelector('.not_my_asks')

/**
Attaches event listeners to butttons
@param {string} button - var name assigned to button
@param {string} content - var name assigned to content div
@param {string} other - var name assigned to another content div
*/
/*my_asks.style.maxHeight = my_asks.scrollHeight + 'px';
not_my_asks.style.maxHeight = not_my_asks.scrollHeight + 'px';*/
function collapsible(button, content, other){

	content.style.maxHeight = content.scrollHeight + 'px';
	button.addEventListener("click", function() {
	  	this.classList.toggle("active");

	    content.style.maxHeight = content.scrollHeight + 'px';
	    other.style.maxHeight = 0;

	});
}
	allChatsBtn.addEventListener('click', () => {
		my_asks.style.maxHeight = my_asks.scrollHeight + 'px';
		not_my_asks.style.maxHeight = not_my_asks.scrollHeight + 'px';
	})


	collapsible(myAsksBtn, my_asks, not_my_asks);
	collapsible(notMyAsksBtn, not_my_asks, my_asks);

// make the active chat button different

/*let chatListButtons = document.querySelectorAll('.chat-list-btn')
let active_chat = document.querySelector("data")

if (chatListButtons && active_chat) {
	for (let i = 0; i < chatListButtons.length; i++) {
		if (chatListButtons[i].classList.item(0) == active_chat.value) {
			chatListButtons[i].classList.add('active')
		}
	}
}*/

// SET THE BOOK STATUS TO RESERVED

let checkbox = document.getElementById('reserveBook')
if (checkbox) {
	if (book_reserved == 'true') {
		checkbox.checked = true
	} else  {
		checkbox.checked = false
	}
}

if (checkbox) {
	let book_id = checkbox.getAttribute("data")

	checkbox.onchange = function(e) {

		let reserved = e.target.checked
		let status = reserved ? 'reserved' : 'free'

		fetch(`/api/books/${book_id}/update`, {
			method: 'POST',
			body: JSON.stringify({
				status: status,
			}),
			headers: {
				"Content-type": "application/json; charset=UTF-8"
			}
		})
	  .catch(function (error) {
	    console.log(error);
	  });

	}
}
