
const chatForm = document.getElementById('chat-form');
const chatMessages = document.querySelector('.chat-messages');

const socket = io();

// get the cookie info
let cookies = document.cookie.split(';')
let user_id, chat_id, book_reserved

cookies.forEach((el) => {
  if (el.split('=')[0].trim() == 'user') {
    user_id = el.split('=')[1]
  } else if (el.split('=')[0].trim() == 'book_reserved') {
    book_reserved = el.split('=')[1]
  } else if (el.split('=')[0].trim() == 'chatID') {
    chat_id = el.split('=')[1]
  }
})

chatMessages.scrollTop = chatMessages.scrollHeight
// Join chatroom
socket.emit('joinRoom', { user_id, chat_id });

// Message from server
socket.on('message', message => {
  outputMessage(message);
  // Scroll down
  chatMessages.scrollTop = chatMessages.scrollHeight;
});

// Message submit
chatForm.addEventListener('submit', e => {
  e.preventDefault();
  // Get message text
  let msg = {};
  msg.text = e.target.elements.msg.value;
  msg.text = msg.text.trim();
  msg.user = user_id;
  msg.chat_id = chat_id
//console.log(msg)
  //console.log(chat_id)
  if (!msg){
    return false;
  }
  fetch(`/socket/${chat_id}`, {
			method: 'POST',
			body: JSON.stringify({
				message: msg,
			}),
			headers: {
				"Content-type": "application/json; charset=UTF-8"
			}
		})
		.catch(function (error) {
	    console.log(error);
		})
  // Emit message to server
  socket.emit('chatMessage', msg);
  // Clear input
  e.target.elements.msg.value = '';
  e.target.elements.msg.focus();
});

// Output message to DOM
function outputMessage(message) {

  const div = document.createElement('div');
  div.classList.add('message-box');
  const p = document.createElement('div');
  p.classList.add('message');
  if (user_id == message.user) {
    p.classList.add('user-true')
  }
  p.innerHTML += `<span>${message.time}</span>`;
  div.appendChild(p);
  const para = document.createElement('p');
  para.classList.add('text');
  para.innerText = message.text
  p.appendChild(para);

  document.querySelector('.chat-messages').appendChild(div);
}

