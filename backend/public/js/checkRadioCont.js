
let condValue = document.querySelectorAll("data")
let radiosArray =  document.querySelectorAll('.radio_cond')

if (condValue.length && radiosArray.length) {
	for (let i = 0; i < condValue.length; i++) {

		for (let j = 0; j < radiosArray.length; j++) {

			if (radiosArray[j].classList.item(1) == condValue[i].classList.item(1) && radiosArray[j].value == condValue[i].value) {
				radiosArray[j].checked = true;
				break;
			}
		}
	}
}

// SET THE BOOK STATUS TO RESERVED

let checkbox = document.querySelectorAll('.reserveBook')

if (checkbox.length) {
		for (i=0;i<checkbox.length;i++) {

			let book_id = checkbox[i].getAttribute("data")
			let data_status = checkbox[i].getAttribute("data-status")

			if (data_status == 'reserved') {
				checkbox[i].checked = true
			} else {checkbox[i].checked = false}


		checkbox[i].onchange = function(e) {
			let reserved = e.target.checked
			let status = reserved ? 'reserved' : 'free'

			fetch(`/api/books/${book_id}/update`, {
				method: 'POST',
				body: JSON.stringify({
					status: status,
				}),
				headers: {
					"Content-type": "application/json; charset=UTF-8"
				}
			})
			.then(console.log(`status changed to ${status}`))
			.then(alert(`status changed to ${status}`))
			.catch(function (error) {
		    console.log(error);
		  });
		}
	}

}
