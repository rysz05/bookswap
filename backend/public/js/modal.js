/*import '../css/modal.css';
import '../css/bookswap_maincss.css'*/
/*require('../css/modal.css')
require('../css/bookswap_maincss.css')*/
// open buttons
let addressModalBtn = document.getElementById('addressModalBtn');
let statsModalBtn = document.getElementById('statsModalBtn')
let nextModalBtn = document.getElementById('nextModalBtn');
let anotherModalBtn = document.getElementById('anotherModalBtn')
let persoModalBtn = document.getElementById('persoModalBtn')
// modals
let addressModal = document.getElementById("addressModal")
let statsModal = document.getElementById("statsModal")
let nextModal = document.getElementById("nextModal")
let anotherModal = document.getElementById("anotherModal")
let persoModal = document.getElementById("persoModal")

	attachModaltoButtons('c_addressModal', addressModalBtn, addressModal)

	attachModaltoButtons('c_statsModal', statsModalBtn, statsModal)

	attachModaltoButtons('c_nextModal', nextModalBtn, nextModal)

	attachModaltoButtons('c_anotherModal', anotherModalBtn, anotherModal)


if (persoModalBtn && persoModal) {
	attachModaltoButtons('c_persoModal', persoModalBtn, persoModal)
}

function attachModaltoButtons(c_btnId, btn, modal) {
if (btn) {

	btn.onclick = () => {openModal(modal)}
	document.getElementById(`${c_btnId}`).onclick = () => {closeModal(modal)}
}
}

function openModal(modal) {
    document.getElementById("backdrop").style.display = "block"
    modal.style.display = "block"
}
function closeModal(modal) {
    document.getElementById("backdrop").style.display = "none"
    modal.style.display = "none"
}
