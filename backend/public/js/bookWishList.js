// drag and drop fucntion


function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
	ev.ondragstart
	//console.log(ev.target.parentElement)
	let objectToPass = {status: ev.target.getAttribute("data_status"),
						id: ev.target.id,
						parent_id: ev.target.parentElement.id}
				/*		{target: ev.target}*/
	let stringToSend = JSON.stringify(objectToPass)
	//console.log(stringTo)
  ev.dataTransfer.setData("text", stringToSend);
}

function drop(ev) {
  ev.preventDefault();

	let data = JSON.parse(ev.dataTransfer.getData("text"))

  if (ev.target.className.match(/dropbox/) && ev.target.id !== data.parent_id) {

	ev.target.appendChild(document.getElementById(data.id));

	let status = data.status == "read" ? "toBeRead" : "read"
	let book = {
	  	id: data.id,
	  	status: status
	  }

	 fetch(`/api/books/${data.id}/update`, {
            method: 'POST',
            body: JSON.stringify(book),
            headers: {
            "Content-type": "application/json; charset=UTF-8"
        },
       })
	 document.getElementById(data.id).setAttribute("data_status", status)
  }
}

// clearing the read pile

let clear = document.getElementById("clearAll")

if (clear) clear.addEventListener("click", clearAll)

function clearAll() {
	let children = document.getElementById("clearAllBox").children;
	let idArr = [];
	// remove from the DOM
	for (let i = 0; i < children.length; i++) {
		if (children[i].id) idArr.push(children[i]);
	}
	idArr.forEach((el) => el.remove())

	// remove from the database
	fetch(`/api/books/clear`, {
	            method: 'POST',
	            headers: {"Content-type": "application/json; charset=UTF-8"},
	       })
}
// check which dropbox (if it moved r not)
