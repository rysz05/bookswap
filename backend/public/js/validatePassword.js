var password = document.querySelector(".password_input")
  , confirm_password = document.querySelector(".r_password_input");

function validatePassword(){

  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}
if (password){
	password.onchange = validatePassword;
}
if (confirm_password) {
	confirm_password.onkeyup = validatePassword;
}
