let rememberBtn = document.getElementById('rememberMe_btn')
if (rememberBtn) {
  rememberBtn.addEventListener('click', lsRememberMe)
}

const rmCheck = document.getElementById("rememberMe"),
    emailInput = document.getElementById("inputUsername");
    passInput = document.getElementById("password_input")


if(rmCheck) {
  if (localStorage.checkbox && localStorage.checkbox !== "") {
    rmCheck.setAttribute("checked", "checked");
    emailInput.value = localStorage.username;
    passInput.value = localStorage.password
  } else {
    rmCheck.removeAttribute("checked");
    emailInput.value = "";
    passInput.value = "";
  }
}

function lsRememberMe() {
  if (rmCheck.checked && emailInput.value !== "") {
    localStorage.username = emailInput.value;
    localStorage.password = passInput.value;
    localStorage.checkbox = rmCheck.value;
  } else {
    localStorage.username = "";
    localStorage.password = "";
    localStorage.checkbox = "";
  }
}
