const faker = require("faker");
const Seeder = require("mysql-db-seed").Seeder;
require('dotenv').config()
const bcrypt = require('bcrypt');
const db = require('../../modules/db.js')

faker.locale = "en";

const seed = new Seeder(
  10,
  process.env.DB_HOST,
  process.env.DB_USER,
  process.env.DB_PASS,
  process.env.DB_DATABASE
);

	(async () => {
	  await seed.seed(
	    30,
	    "users",
	    {
		    name: faker.name.firstName,
		    email: faker.internet.email,
		    img_path: faker.image.people,
		    password: await bcrypt.hash("p", 10),
		    city: faker.address.city,
		    street: faker.address.streetName,
		    building_nr: faker.random.number,
		    lat: faker.address.latitude,
		    lng: faker.address.longitude,
		    phone_nr: faker.phone.phoneNumber,
		    created_at: seed.nativeTimestamp(),
		    updated_at: seed.nativeTimestamp(),
		    rating: 3
	    }
	  )
	  await seedBooks()
	  seed.exit();
	  process.exit();
	})();

	async function seedBooks() {
		//try {
			let idArray = await db.selectAllIds('users')
				console.log(idArray)
			for (let el of idArray) {
			  await seed.seed(
			    5,
			    "books",
			    {
				    author: faker.name.findName,
				    title: faker.lorem.words,
				    genre: "fantasy",
				    state: "free",
				    quote: faker.lorem.paragraph,
				    book_condition: "good",
				    status: "free",
				    user: el.id,
				    created_at: seed.nativeTimestamp(),
				    updated_at: seed.nativeTimestamp()
			    }
			  )

			}

		  seed.exit();
		/*} catch (e) {console.log(e)}*/
		  process.exit();
		}


