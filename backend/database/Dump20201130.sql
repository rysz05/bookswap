-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: test_db
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
CREATE TABLE `books` (
  `id` int NOT NULL AUTO_INCREMENT,
  `author` varchar(100) NOT NULL,
  `title` varchar(45) NOT NULL,
  `genre` varchar(100) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `quote` longtext,
  `book_condition` varchar(45) DEFAULT 'good',
  `status` varchar(45) NOT NULL DEFAULT 'free',
  `user` int NOT NULL,
  PRIMARY KEY (`id`,`author`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `id_idx` (`user`),
  CONSTRAINT `user_id` FOREIGN KEY (`user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES (85,'Ayn Rand','The Fountainhead',NULL,NULL,NULL,'good','free',93),(86,'Terry Pratchett','Potworny regiment','Powies c  fantastyczna angielska',NULL,NULL,'good','free',93),(87,'Fix it later','Fale',NULL,NULL,NULL,'good','free',91),(88,'Fix it later','City on Fire','Fiction, historical',NULL,NULL,'good','free',91),(89,'Fix it later','Cien años de soledad. - 4. edición',NULL,NULL,NULL,'good','free',90),(90,'Fix it later','If Cats Disappeared From The World',NULL,NULL,NULL,'good','free',90),(91,'Terry Pratchett','Potworny regiment','Powies c  fantastyczna angielska',NULL,NULL,'good','free',92),(92,'Fix it later','Fale',NULL,NULL,NULL,'good','free',92),(93,'Fix it later','We are Not Ourselves',NULL,NULL,NULL,'good','free',97),(94,'Fix it later','Cien años de soledad. - 4. edición',NULL,NULL,NULL,'good','free',97),(95,'Fix it later','If Cats Disappeared From The World',NULL,NULL,NULL,'good','free',98),(96,'Fix it later','Fale',NULL,NULL,NULL,'good','free',98),(97,'Fix it later','Fale',NULL,NULL,NULL,'good','free',99),(98,'Ayn Rand','The Fountainhead',NULL,NULL,NULL,'good','free',99),(99,'Fix it later','We are Not Ourselves',NULL,NULL,'sfgfag','good','free',96),(100,'Terry Pratchett','Potworny regiment','Powies c  fantastyczna angielska',NULL,NULL,'good','free',96),(101,'Fix it later','City on Fire','Fiction, historical',NULL,NULL,'good','free',95),(102,'Fix it later','If Cats Disappeared From The World',NULL,NULL,NULL,'good','free',95),(103,'Ayn Rand','The Fountainhead',NULL,NULL,NULL,'good','free',94),(104,'Terry Pratchett','Potworny regiment','Powies c  fantastyczna angielska',NULL,NULL,'good','free',94),(105,'Fix it later','Fale',NULL,NULL,NULL,'good','free',96),(106,'Fix it later','We are Not Ourselves',NULL,NULL,NULL,'good','free',109),(107,'Fix it later','Blady król',NULL,NULL,NULL,'good','free',109),(108,'Fix it later','Cien años de soledad. - 4. edición',NULL,NULL,NULL,'good','free',109);
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chats`
--

DROP TABLE IF EXISTS `chats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
CREATE TABLE `chats` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_user` int NOT NULL,
  `second_user` int NOT NULL,
  `book_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`first_user`),
  KEY `chat_user2_idx` (`second_user`),
  CONSTRAINT `chat_user` FOREIGN KEY (`first_user`) REFERENCES `users` (`id`),
  CONSTRAINT `chat_user2` FOREIGN KEY (`second_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chats`
--

LOCK TABLES `chats` WRITE;
/*!40000 ALTER TABLE `chats` DISABLE KEYS */;
INSERT INTO `chats` VALUES (51,96,99,97),(52,94,92,NULL),(53,105,96,NULL),(54,107,93,NULL),(55,107,95,NULL),(56,109,96,NULL),(57,109,93,NULL),(58,105,109,NULL),(59,93,95,NULL),(108,93,90,NULL),(109,93,99,NULL),(110,94,95,NULL),(111,94,99,NULL),(112,94,98,NULL),(113,94,96,NULL),(114,97,93,NULL);
/*!40000 ALTER TABLE `chats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
CREATE TABLE `messages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `text` longtext NOT NULL,
  `time` datetime DEFAULT NULL,
  `user` varchar(45) NOT NULL,
  `chat_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `chat_id_idx` (`chat_id`),
  CONSTRAINT `chat_id` FOREIGN KEY (`chat_id`) REFERENCES `chats` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=292 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (269,'wsgv','2020-11-30 18:51:56','93',57),(270,'yki','2020-11-30 18:53:30','93',109),(271,'yyyyyyyyy','2020-11-30 18:53:44','93',109),(272,'yyyyyyyyy','2020-11-30 18:54:57','93',109),(273,'yyyyyyyyy','2020-11-30 18:56:29','93',109),(274,'jmhhvmhv','2020-11-30 18:56:37','93',109),(275,'gwsge','2020-11-30 19:12:25','94',111),(276,'sfhsfg','2020-11-30 19:13:17','94',112),(277,'srhfhg','2020-11-30 19:13:27','94',113),(278,'frhrgde','2020-11-30 19:13:29','94',113),(279,'sdbgvfs','2020-11-30 19:13:55','94',112),(280,'dsfzd','2020-11-30 19:13:57','94',112),(281,'fthytu','2020-11-30 19:15:14','97',114),(282,'guo','2020-11-30 19:15:42','94',113),(283,'fthytu','2020-11-30 19:17:14','97',114),(284,'[object Object]','2020-11-30 19:19:46','94',113),(285,'[object Object]','2020-11-30 19:19:48','94',113),(286,'[object Object]','2020-11-30 19:27:45','94',113),(287,'[object Object]','2020-11-30 19:29:31','94',113),(288,'[object Object]','2020-11-30 19:29:34','94',113),(289,'[object Object]','2020-11-30 19:30:02','94',113),(290,'[object Object]','2020-11-30 19:30:05','94',113),(291,'[object Object]','2020-11-30 19:30:08','94',113);
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `img_path` varchar(50) DEFAULT NULL,
  `password` char(150) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `street` varchar(45) DEFAULT NULL,
  `building_nr` int DEFAULT NULL,
  `lat` varchar(45) DEFAULT NULL,
  `lng` varchar(45) DEFAULT NULL,
  `phone_nr` varchar(45) DEFAULT NULL,
  `accessTokenFB` varchar(200) DEFAULT NULL,
  `accessTokenGoogle` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (90,'Asia','asia@asia',NULL,'$2b$10$sqYPKi8GjELkmqgKOpI0Pu7ENuWXzpAevGDZqL3uIIWVZ6cA4ngUm','Krapkowice','Moniuszki',11,'50.4747413','17.9675262','512365847',NULL,NULL),(91,'Karol','karol@k',NULL,'$2b$10$bBQ0ktmZZNKjN13wi7JCDevM9FaBZdSTeigsQGSE1rnDz/POQK8Y.','Gdańsk','Kolejowa',5,'54.347629','18.6452324','475859885',NULL,NULL),(92,'Ania','ania@a',NULL,'$2b$10$uulsFGK2sVs6JYg5Gni9XeodWHXZCGWRabm9Y78pJDYvCvNB58OC6','Chałupy','Bosmańska',2,'51.0543372','19.8804472','411256544',NULL,NULL),(93,'Maciek','maciek@m',NULL,'$2b$10$VzRcScuqCzD7frc3LxnYG.Ski4UUpW0G4tsKzG7GykXCHjlqr5wQi','Zakopane','Nowatorska',14,'49.2969446','19.950659','555555555',NULL,NULL),(94,'Helena','hela@h',NULL,'$2b$10$XUxEk2jk1g4OKBRKuHWwEumWzBWQGpA.vNf0zL3RsaOMzAP18rM0G','Warszawa','Krucza',50,'52.2314458','21.0167963','547858451',NULL,NULL),(95,'Krystian','krys@k',NULL,'$2b$10$9hrUxnHMzEqBgt6KFo7nOu1gbYu0p.S0ocAvPf7uv.IDUNZe6GE7O','Warszawa','Nowowiejska',10,'52.2200497','21.0151772','542656958',NULL,NULL),(96,'Paweł','paw@p',NULL,'$2b$10$qs3xkU6lLUtOoICdPYI3ouzymXfQ4AWwdDMIy8ibnzawHPpky0GDq','Warszawa','Krucza',47,'52.3165849','21.000105','854787987',NULL,NULL),(97,'piotr','piotr@p',NULL,'$2b$10$mmc9oMN.dY9e/hUFultJverXQyXRpIPft5mDWzNz2abF0HI1exqRi','Warszawa','Solurska',5,'52.1752189','21.1945008','585454236',NULL,NULL),(98,'malwina','mal@m',NULL,'$2b$10$e1B9d184nwwyiZn7eEJwN.cBfhEq207XuQK9gcfhZtRoatC8xpyI.','Warszawa','Kłopotowskiego',14,'52.249921','21.031007','142253652',NULL,NULL),(99,'nowy','nowy@t',NULL,'$2b$10$DMqhy1KHBpEoAAOMs1wrZOQBzj7V/WQKlGyRqs1RGHuP8L38jrBd.','Warszawa','Karmelicka',3,'52.2444915','20.9945876','584655963',NULL,NULL),(105,'Justyna Rysz',NULL,NULL,NULL,'Warszawa','Wilanowska',5,'52.2305267','21.0401372',NULL,'2748467308734773',NULL),(106,'j r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'110943591820584890431',NULL),(107,'j r',NULL,NULL,NULL,'Warszawa','Piłsudskiego',2,'52.2423542','21.0115858',NULL,NULL,'110943591820584890431'),(108,'d','d@d',NULL,'$2b$10$lLd5jwcY5bYIMpG1B.0pTuj1KJ9EcwgxUr.NUaK9CmU4MKch5fQvO','Warszawa','Świętokrzyska',10,'52.2350954','21.0078988','222222222',NULL,NULL),(109,'e','d@w','uploads\\20200715_184440.jpg','$2b$10$DjrmAyXhPWPbYbLTE3HYt.4Vo9N9eL4iIfxHSNCSgHU71k2J9m0/K','Warszawa','Podbipięty',5,'52.1758698','21.0281766','555888999',NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-30 22:40:26
