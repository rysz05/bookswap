'use strict';
//const seed = require ('../seeding/mysql-db-seed')
var dbm;
/*var type;
var seed;*/
/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
/*  type = dbm.dataType;
  seed = seedLink;*/
};


/* Promise-based version */
exports.up = function (db) {
  if (db.users) {db.dropTable('users')}
  return db.createTable('users', {
      id: { type: 'int', primaryKey: true, notNull: true, autoIncrement: true},
      name: {type : 'string', notNull: true},
      email: {type : 'string'},
      img_path: {type : 'string'},
      password: {type : 'string'},
      city: {type : 'string'},
      street: {type : 'string'},
      building_nr: {type : 'int'},
      lat: {type : 'string'},
      lng: {type : 'string'},
      phone_nr: {type : 'string'},
      accessTokenFB: {type : 'string'},
      accessTokenGoogle: {type : 'string'},
      created_at: {type : 'string'},
      updated_at: {type : 'string'},
      rating: {type: 'int'},
      authToken: {type: "string"},
      authStatus: {type: "string", defaultValue: 'not_valid'},
      secureCode: {type: 'int'},
      randNumb: {type: 'string'}
    })
};

exports.down = function (db) {
  return db.dropTable('users');
};

exports._meta = {
  "version": 1
};
