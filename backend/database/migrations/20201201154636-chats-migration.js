'use strict';

var dbm;
/*var type;
var seed;*/

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
/*  type = dbm.dataType;
  seed = seedLink;*/
};

exports.up = function(db) {
  if (db.chats) {db.dropTable('chats')}
  return db.createTable('chats', {
  	id: {type:'int', autoIncrement: true, primaryKey: true, notNull: true},
  	first_user: {type: 'int', notNull: true, foreignKey: {
  		name: 'chat_user',
  		table: 'users',
      rules: {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
      mapping: 'id'}
    },
  	second_user: {type: 'int', notNull: true, foreignKey: {
  		name: 'chat_user2',
  		table: 'users',
      rules: {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
      mapping: 'id'}
  	},
  	book_id: {type: 'int'},
    created_at: {type : 'string'},
    updated_at: {type : 'string'}
  })
};

exports.down = function(db) {
  return db.dropTable('chats')
};

exports._meta = {
  "version": 1
};
