require('dotenv').config()

let URL = process.env.URL

function regValidationEmail(name, email, token, user_id) {

  const mailjet = require ('node-mailjet').connect(
    process.env.MJ_APIKEY_PUBLIC,
    process.env.MJ_APIKEY_PRIVATE
  )
  const request = mailjet
    .post("send", {'version': 'v3.1'})
    .request({
    "Messages":[
        {
            "From": {
                "Email": "registration@bookswap.io",
                "Name": "Bookswap Team"
            },
            "To": [
                {
                    "Email": email,
                    "Name": name
                }
            ],
            "Subject": "Validation email",
            "TextPart": `Dear ${name}, welcome to Bookswap!`,
            "HTMLPart": `<h3>Thank you for registering at Bookswap.io!</h3><br /> Please take a moment now to confirm your account by clicking this link:<br /><a href=\"${URL}/${user_id}/validate/${token}\">https://bookswap.io/${user_id}/validate/${token}</a><br />If you have any troubles, you can also copy and paste the link into your browser.`
        }
    ]
  })
  request
    .then((result) => {
      //console.log(result.body.Messages[0].To)
      console.log(result.body.Messages[0].To)
    })
    .catch((err) => {
      console.log(err.statusCode)
    })
}

function newUserMailNotification(user_name, time) {

  const mailjet = require ('node-mailjet').connect(
    process.env.MJ_APIKEY_PUBLIC,
    process.env.MJ_APIKEY_PRIVATE
  )
  const request = mailjet
    .post("send", {'version': 'v3.1'})
    .request({
    "Messages":[
        {
            "From": {
                "Email": "registration@bookswap.io",
                "Name": "Bookswap Registration"
            },
            "To": [
                {
                    "Email": 'jusrysz05@gmail.com',
                    "Name": 'Justyna'
                }
            ],
            "Subject": "New user registered to Bookswap",
            "HTMLPart": `New user ${user_name} registered at ${time}`
        }
    ]
  })
  request
    .then((result) => {
      console.log(result.body.Messages[0].To)
    })
    .catch((err) => {
      console.log(err.statusCode)
    })
}

function forgotPassword(user, code) {

  const mailjet = require ('node-mailjet').connect(
    process.env.MJ_APIKEY_PUBLIC,
    process.env.MJ_APIKEY_PRIVATE
  )
  const request = mailjet
    .post("send", {'version': 'v3.1'})
    .request({
    "Messages":[
        {
            "From": {
                "Email": "security@bookswap.io",
                "Name": "Bookswap"
            },
            "To": [
                {
                    "Email": user.email,
                    "Name": user.name
                }
            ],
            "Subject": `${code} is your Bookswap account security code`,
            "TextPart": `Hi ${user.name},`,
            "HTMLPart": `<p>We received a request to reset your Bookswap password.<br />Enter the following password reset code:</p><br /><h2>${code}</h2><br /><p>Didn't request this change?<br />If you didn't request a new password simply ignore this email</p>`
        }
    ]
  })
  request
    .then((result) => {
      //console.log(result.body.Messages[0].To)
      console.log(result.body.Messages[0].To)
    })
    .catch((err) => {
      console.log(err.statusCode)
    })
}

module.exports = {
  regValidationEmail,
  newUserMailNotification,
  forgotPassword,
}
