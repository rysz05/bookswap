
const pool = require('../database.js')

/**
Adds a new book into the books table
@param {Object} newBook - the new book to add
@returns {Object} The response object
*/
async function addBook(newBook) {
	result = pool.query(`INSERT INTO books SET ?`, newBook)
	return result
}
/**
Updates a book from the books table by id
@param {object} changedData - the changed parameters
@param {number} id - the id of the book to change
@returns {Object} The response object
*/
async function updateBook(changedData, id) {
	result = pool.query(`UPDATE books SET ? WHERE id = ${id}`, changedData)
	return result
}
/**
Delete a book from the books table by id
@param {number} id - the id of the book to delete
@returns {Object} The response object
*/
async function deleteBook(database, id) {
	result = pool.query(`DELETE FROM ${database} WHERE id = ${id}`)
	return result
}
async function getBooksByIdAndStatus(user_id, status) {
	data = await pool.query(`SELECT * FROM wishlistbook WHERE user = "${user_id}" AND status = "${status}"`)
	return data
}

module.exports = {
	addBook,
	updateBook,
	deleteBook,
	getBooksByIdAndStatus
}
