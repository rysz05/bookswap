const db = require('./db.js')

/**
Renders the chat page
@param {number} user_id - the logged user id
@param {Object} chat - the accessed chat object
@returns {Object} all the objects needed to load the page properly
*/
async function renderChatView(user_id, chat) {

		//let chat = await db.findItemBy('chats', 'id', chat.id)

	// shows all messages from this chat
		let data = await db.selectAllById(chat.id);

		let messages = db.setActiveUser(data, user_id)

		let other = await db.setOther(user_id, chat.id)

		// get all chats
		//let allChats = await db.getAllChats()


		let userData = await db.getPersonalData(user_id)
		userData = userData[0]
	// show list of chats
		let asks = await db.getAllChattersNames(user_id)
			my_asks = asks.filter((el) => el.first_user === user_id)
			not_my_asks = asks.filter((el) => el.second_user === user_id)

	// shows other users name
		let personalData = await db.getPersonalData(other);
		personalData = personalData[0];

		let book = await db.findItemBy('books', 'id', chat.book_id)
				book = book[0]

		let renderCheckbox = db.renderBookCheckbox(chat.first_user, user_id)
	return {
		userData,
		personalData,
		my_asks,
		not_my_asks,
		messages,
		renderCheckbox,
		book
	}
}

module.exports = {
	renderChatView
}
